## TIMEQUEST EXAMPLE FILE

# Es mejor asignar nombres a los relojes de los PLL de forma manual 
# por un tema de orden, ocupar derive_pll_clocks genera nombres irreconocibles

create_clock -name "clk50" -period 20.000ns [get_ports CLOCK50]
create_generated_clock -name clock_pll_25 -source [get_ports {CLOCK50}] -divide_by 2 [get_pins {PLL_1|pll_150_25|altera_pll_i|general[1].gpll~PLL_OUTPUT_COUNTER|divclk}]
create_generated_clock -name clock_pll_150 -source [get_ports {CLOCK50}] -multiply_by 3 [get_pins {PLL_1|pll_150_25|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}]

# derive_pll_clocks
derive_clock_uncertainty