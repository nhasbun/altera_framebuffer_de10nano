`include "i2c/I2C_HDMI_Config.v"

module top
(
  // CLOCKS
  input CLOCK50, RESET_N,

  // SWITCH Y LEDS
  input  [3:0]SWITCH_ARRAY_IO,
  output [7:0]LED_ARRAY_IO,

  // QSYS BULLSHIT
  output wire        hps_io_hps_io_emac1_inst_TX_CLK,
  output wire        hps_io_hps_io_emac1_inst_TXD0,
  output wire        hps_io_hps_io_emac1_inst_TXD1,
  output wire        hps_io_hps_io_emac1_inst_TXD2,
  output wire        hps_io_hps_io_emac1_inst_TXD3,
  input  wire        hps_io_hps_io_emac1_inst_RXD0,
  inout  wire        hps_io_hps_io_emac1_inst_MDIO,
  output wire        hps_io_hps_io_emac1_inst_MDC,
  input  wire        hps_io_hps_io_emac1_inst_RX_CTL,
  output wire        hps_io_hps_io_emac1_inst_TX_CTL,
  input  wire        hps_io_hps_io_emac1_inst_RX_CLK,
  input  wire        hps_io_hps_io_emac1_inst_RXD1,
  input  wire        hps_io_hps_io_emac1_inst_RXD2,
  input  wire        hps_io_hps_io_emac1_inst_RXD3,
  output wire [14:0] memory_mem_a,
  output wire [2:0]  memory_mem_ba,
  output wire        memory_mem_ck,
  output wire        memory_mem_ck_n,
  output wire        memory_mem_cke,
  output wire        memory_mem_cs_n,
  output wire        memory_mem_ras_n,
  output wire        memory_mem_cas_n,
  output wire        memory_mem_we_n,
  output wire        memory_mem_reset_n,
  inout  wire [31:0] memory_mem_dq,
  inout  wire [3:0]  memory_mem_dqs,
  inout  wire [3:0]  memory_mem_dqs_n,
  output wire        memory_mem_odt,
  output wire [3:0]  memory_mem_dm,
  input  wire        memory_oct_rzqin,

  // ********************************** //
  // ** HDMI CONNECTIONS **

  // AUDIO
  // SPDIF va desconectado
  output HDMI_I2S0,  // dont care, no se usan
  output HDMI_MCLK,  // dont care, no se usan
  output HDMI_LRCLK, // dont care, no se usan
  output HDMI_SCLK,  // dont care, no se usan

  // VIDEO
  output [23:0] HDMI_TX_D, // RGBchannel
  output HDMI_TX_VS,  // vsync
  output HDMI_TX_HS,  // hsync
  output HDMI_TX_DE,  // dataEnable
  output HDMI_TX_CLK, // vgaClock

  // REGISTERS AND CONFIG LOGIC
  // HPD viene del conector
  input  HDMI_TX_INT,
  inout  HDMI_I2C_SDA,  // HDMI i2c data
  output HDMI_I2C_SCL, // HDMI i2c clock
  // ********************************** //

  // ** UART CONNECTIONS **
  input  UART_RX,
  output UART_TX, UART_TX_DEBUG,

  // ** HC-05 BT_KEY to enter order-responde mode
  output BT_KEY
);

wire [7:0] led_array_io_hps;
wire [23:0] VGA_D;
wire VGA_CLK, VGA_DE, VGA_HS, VGA_VS;

ledtest inst_ledtest
(
  // QSYS BS
  .hps_io_hps_io_emac1_inst_TX_CLK (hps_io_hps_io_emac1_inst_TX_CLK),
  .hps_io_hps_io_emac1_inst_TXD0   (hps_io_hps_io_emac1_inst_TXD0),
  .hps_io_hps_io_emac1_inst_TXD1   (hps_io_hps_io_emac1_inst_TXD1),
  .hps_io_hps_io_emac1_inst_TXD2   (hps_io_hps_io_emac1_inst_TXD2),
  .hps_io_hps_io_emac1_inst_TXD3   (hps_io_hps_io_emac1_inst_TXD3),
  .hps_io_hps_io_emac1_inst_RXD0   (hps_io_hps_io_emac1_inst_RXD0),
  .hps_io_hps_io_emac1_inst_MDIO   (hps_io_hps_io_emac1_inst_MDIO),
  .hps_io_hps_io_emac1_inst_MDC    (hps_io_hps_io_emac1_inst_MDC),
  .hps_io_hps_io_emac1_inst_RX_CTL (hps_io_hps_io_emac1_inst_RX_CTL),
  .hps_io_hps_io_emac1_inst_TX_CTL (hps_io_hps_io_emac1_inst_TX_CTL),
  .hps_io_hps_io_emac1_inst_RX_CLK (hps_io_hps_io_emac1_inst_RX_CLK),
  .hps_io_hps_io_emac1_inst_RXD1   (hps_io_hps_io_emac1_inst_RXD1),
  .hps_io_hps_io_emac1_inst_RXD2   (hps_io_hps_io_emac1_inst_RXD2),
  .hps_io_hps_io_emac1_inst_RXD3   (hps_io_hps_io_emac1_inst_RXD3),
  .memory_mem_a                    (memory_mem_a),
  .memory_mem_ba                   (memory_mem_ba),
  .memory_mem_ck                   (memory_mem_ck),
  .memory_mem_ck_n                 (memory_mem_ck_n),
  .memory_mem_cke                  (memory_mem_cke),
  .memory_mem_cs_n                 (memory_mem_cs_n),
  .memory_mem_ras_n                (memory_mem_ras_n),
  .memory_mem_cas_n                (memory_mem_cas_n),
  .memory_mem_we_n                 (memory_mem_we_n),
  .memory_mem_reset_n              (memory_mem_reset_n),
  .memory_mem_dq                   (memory_mem_dq),
  .memory_mem_dqs                  (memory_mem_dqs),
  .memory_mem_dqs_n                (memory_mem_dqs_n),
  .memory_mem_odt                  (memory_mem_odt),
  .memory_mem_dm                   (memory_mem_dm),
  .memory_oct_rzqin                (memory_oct_rzqin),

  // USEFUL SHIT
  .clk_clk                         (CLOCK50),
  .led_array_io_export             (led_array_io_hps),
  .switch_array_io_export          (SWITCH_ARRAY_IO),

  // VGA OUTPUT
  .clocked_video_vid_clk           (clock25),
  .clocked_video_vid_data          (VGA_D),
  .clocked_video_underflow         (),
  .clocked_video_vid_datavalid     (VGA_DE),
  .clocked_video_vid_v_sync        (VGA_VS),
  .clocked_video_vid_h_sync        (VGA_HS),
  .clocked_video_vid_f             (),
  .clocked_video_vid_h             (),
  .clocked_video_vid_v             (),
  .video_out_status_update_irq_irq (),

  // UART 16550 CONNECTIONS
  .uart_16550_irq_sender_irq        (),
  .uart_16550_rs_232_serial_sin     (UART_RX),
  .uart_16550_rs_232_serial_sout    (UART_TX),
  .uart_16550_rs_232_serial_sout_oe (),
  .uart_16550_rs_232_modem_cts_n    (),
  .uart_16550_rs_232_modem_rts_n    (),
  .uart_16550_rs_232_modem_dsr_n    (),
  .uart_16550_rs_232_modem_dcd_n    (),
  .uart_16550_rs_232_modem_ri_n     (),
  .uart_16550_rs_232_modem_dtr_n    (),
  .uart_16550_rs_232_modem_out1_n   (),
  .uart_16550_rs_232_modem_out2_n   ()
);

// ** PLL 200 ** //
// child clock 25MHz
wire clock150, locked_pll_1;
wire clock25;

PLL PLL_1
(
  .clock50_clk              (CLOCK50),
  .reset_reset_n            (RESET_N),
  .pll_150_25_outclk0_clk   (clock150),
  .pll_150_25_locked_export (locked_pll_1),
  .pll_150_25_outclk1_clk   (clock25)
);

// ** LED BEHAVIOR ** //
assign LED_ARRAY_IO[7:3] = led_array_io_hps[7:3];
assign LED_ARRAY_IO[2] = led_array_io_hps[2] || ready_i2c;
assign LED_ARRAY_IO[1] = led_array_io_hps[1] || counter_max;
assign LED_ARRAY_IO[0] = led_array_io_hps[0] || locked_pll_1;

// ** DUMMY FUNCTIONALITY FOR clock150 ** //
integer counter; initial counter = 0;

parameter PWM_VALUE = 10;

always @(posedge clock150 or negedge locked_pll_1) begin
  if(~locked_pll_1) counter <= 0;
  else begin
    if(counter == PWM_VALUE)
      counter <= 0;
    else
      counter <= counter + 1'b1;
  end
end

wire counter_max;
assign counter_max = (counter == PWM_VALUE)? 1 : 0;

// **I2C Interface for ADV7513 initial config**
wire ready_i2c;

I2C_HDMI_Config #(
  .CLK_Freq (50000000), // trabajamos con reloj de 50MHz
  .I2C_Freq (20000)    // reloj de 20kHz for i2c clock
  )

  I2C_HDMI_Config (
  .iCLK        (CLOCK50),
  .iRST_N      (RESET_N),
  .I2C_SCLK    (HDMI_I2C_SCL),
  .I2C_SDAT    (HDMI_I2C_SDA),
  .HDMI_TX_INT (HDMI_TX_INT),
  .READY       (ready_i2c)
);

// HDMI OUTPUTS
assign HDMI_TX_HS  = VGA_HS;
assign HDMI_TX_VS  = VGA_VS;
assign HDMI_TX_D   = VGA_D;
assign HDMI_TX_CLK = clock25;
assign HDMI_TX_DE  = VGA_DE;

// UART DEBUG
assign UART_TX_DEBUG = UART_TX;

endmodule