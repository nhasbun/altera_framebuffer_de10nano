
module ledtest (
	clk_clk,
	clocked_video_vid_clk,
	clocked_video_vid_data,
	clocked_video_underflow,
	clocked_video_vid_datavalid,
	clocked_video_vid_v_sync,
	clocked_video_vid_h_sync,
	clocked_video_vid_f,
	clocked_video_vid_h,
	clocked_video_vid_v,
	frame_buffer_control_interrupt_irq,
	hps_io_hps_io_emac1_inst_TX_CLK,
	hps_io_hps_io_emac1_inst_TXD0,
	hps_io_hps_io_emac1_inst_TXD1,
	hps_io_hps_io_emac1_inst_TXD2,
	hps_io_hps_io_emac1_inst_TXD3,
	hps_io_hps_io_emac1_inst_RXD0,
	hps_io_hps_io_emac1_inst_MDIO,
	hps_io_hps_io_emac1_inst_MDC,
	hps_io_hps_io_emac1_inst_RX_CTL,
	hps_io_hps_io_emac1_inst_TX_CTL,
	hps_io_hps_io_emac1_inst_RX_CLK,
	hps_io_hps_io_emac1_inst_RXD1,
	hps_io_hps_io_emac1_inst_RXD2,
	hps_io_hps_io_emac1_inst_RXD3,
	jtag_debug_master_reset_reset,
	led_array_io_export,
	memory_mem_a,
	memory_mem_ba,
	memory_mem_ck,
	memory_mem_ck_n,
	memory_mem_cke,
	memory_mem_cs_n,
	memory_mem_ras_n,
	memory_mem_cas_n,
	memory_mem_we_n,
	memory_mem_reset_n,
	memory_mem_dq,
	memory_mem_dqs,
	memory_mem_dqs_n,
	memory_mem_odt,
	memory_mem_dm,
	memory_oct_rzqin,
	pll_0_locked_export,
	switch_array_io_export,
	video_out_status_update_irq_irq,
	uart_16550_irq_sender_irq,
	uart_16550_rs_232_serial_sin,
	uart_16550_rs_232_serial_sout,
	uart_16550_rs_232_serial_sout_oe,
	uart_16550_rs_232_modem_cts_n,
	uart_16550_rs_232_modem_rts_n,
	uart_16550_rs_232_modem_dsr_n,
	uart_16550_rs_232_modem_dcd_n,
	uart_16550_rs_232_modem_ri_n,
	uart_16550_rs_232_modem_dtr_n,
	uart_16550_rs_232_modem_out1_n,
	uart_16550_rs_232_modem_out2_n);	

	input		clk_clk;
	input		clocked_video_vid_clk;
	output	[23:0]	clocked_video_vid_data;
	output		clocked_video_underflow;
	output		clocked_video_vid_datavalid;
	output		clocked_video_vid_v_sync;
	output		clocked_video_vid_h_sync;
	output		clocked_video_vid_f;
	output		clocked_video_vid_h;
	output		clocked_video_vid_v;
	output		frame_buffer_control_interrupt_irq;
	output		hps_io_hps_io_emac1_inst_TX_CLK;
	output		hps_io_hps_io_emac1_inst_TXD0;
	output		hps_io_hps_io_emac1_inst_TXD1;
	output		hps_io_hps_io_emac1_inst_TXD2;
	output		hps_io_hps_io_emac1_inst_TXD3;
	input		hps_io_hps_io_emac1_inst_RXD0;
	inout		hps_io_hps_io_emac1_inst_MDIO;
	output		hps_io_hps_io_emac1_inst_MDC;
	input		hps_io_hps_io_emac1_inst_RX_CTL;
	output		hps_io_hps_io_emac1_inst_TX_CTL;
	input		hps_io_hps_io_emac1_inst_RX_CLK;
	input		hps_io_hps_io_emac1_inst_RXD1;
	input		hps_io_hps_io_emac1_inst_RXD2;
	input		hps_io_hps_io_emac1_inst_RXD3;
	output		jtag_debug_master_reset_reset;
	output	[7:0]	led_array_io_export;
	output	[14:0]	memory_mem_a;
	output	[2:0]	memory_mem_ba;
	output		memory_mem_ck;
	output		memory_mem_ck_n;
	output		memory_mem_cke;
	output		memory_mem_cs_n;
	output		memory_mem_ras_n;
	output		memory_mem_cas_n;
	output		memory_mem_we_n;
	output		memory_mem_reset_n;
	inout	[31:0]	memory_mem_dq;
	inout	[3:0]	memory_mem_dqs;
	inout	[3:0]	memory_mem_dqs_n;
	output		memory_mem_odt;
	output	[3:0]	memory_mem_dm;
	input		memory_oct_rzqin;
	output		pll_0_locked_export;
	input	[3:0]	switch_array_io_export;
	output		video_out_status_update_irq_irq;
	output		uart_16550_irq_sender_irq;
	input		uart_16550_rs_232_serial_sin;
	output		uart_16550_rs_232_serial_sout;
	output		uart_16550_rs_232_serial_sout_oe;
	input		uart_16550_rs_232_modem_cts_n;
	output		uart_16550_rs_232_modem_rts_n;
	input		uart_16550_rs_232_modem_dsr_n;
	input		uart_16550_rs_232_modem_dcd_n;
	input		uart_16550_rs_232_modem_ri_n;
	output		uart_16550_rs_232_modem_dtr_n;
	output		uart_16550_rs_232_modem_out1_n;
	output		uart_16550_rs_232_modem_out2_n;
endmodule
