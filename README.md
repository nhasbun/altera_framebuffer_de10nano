# Altera Framebuffer Implementation



Altera Framebuffer IP Core implementation for a simple video system on a de10nano board. The buffer is stored in a DDR3 memory space using the **FPGA-to-HPS SDRAM** interface.

This basic system allow to easily create a software based management video system -- under Linux -- within a restricted space of memory while the FPGA continuously read this space of memory to create a projection on screen.

## Important Notice

For this system to work we need to follow some steps to correctly activate the **FPGA-to-HPS SDRAM** interface. Could be useful to accomplish this to take a look at this [commit](https://github.com/nhasbun/de10nano_ledtest/commit/3726a1d704530bf0dec27f32787a7b96c333b3d1).

You will need to manually activate some clocks on Qsys:

![activate_interface](_readme_images/activate_interface.png)

Then you need to address the activation of this interface at pre-loader or U-boot times. For this I recommend to make some modifications to the GHRD delivered by Terasic and deploy the updates required to the pre-loader. **You need to deploy a FPGA *rbf* configuration that wakes up this interface when you power on the device** then you can use any *sof* file or FPGA reconfiguration that use the same **FPGA-to-HPS SDRAM** interface.

## Recommended Sources

* [How can I enable the FPGA2SDRAM bridge on Cyclone V SOC and Arria V SOC devices?](https://www.altera.com/support/support-resources/knowledge-base/embedded/2016/how-and-when-can-i-enable-the-fpga2sdram-bridge-on-cyclone-v-soc.html)





